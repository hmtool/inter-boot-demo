package tech.mhuang.interchan;

import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.ext.interchan.autoconfiguration.datasecure.annation.DataSecureField;
import tech.mhuang.ext.interchan.autoconfiguration.datasecure.annation.DecryptMapping;
import tech.mhuang.ext.interchan.autoconfiguration.datasecure.annation.EncryptMapping;
import tech.mhuang.ext.interchan.autoconfiguration.datasecure.consts.DecryptType;
import tech.mhuang.ext.interchan.autoconfiguration.datasecure.consts.EncryptType;
import tech.mhuang.ext.interchan.protocol.Result;

@RestController
@RequestMapping("/test")
public class TestController {

    @EncryptMapping
    @GetMapping("/encrypt")
    public Result ajax() {
        return Result.ok("123456");
    }

    @DecryptMapping
    @PostMapping("/decrypt")
    public Result decrypt(@RequestBody Result result) {
        return result;
    }

    @EncryptMapping("hmtool")
    @GetMapping("/hmtool/encrypt")
    public Result hmtoolAjax() {
        return Result.ok("123456");
    }

    @DecryptMapping("hmtool")
    @PostMapping("/hmtool/decrypt")
    public Result hmtoolDecrypt(@RequestBody Result result) {
        return result;
    }

    @EncryptMapping(type = EncryptType.FIELD)
    @DecryptMapping(type = DecryptType.FIELD)
    @PostMapping("/field/decrypt")
    public T1 fieldDecrypt(@RequestBody T1 t) {
        System.out.println(t.getId());
        return t;
    }

    @Data
    static class T1 {
        @DataSecureField
        private String id;

    }
}
